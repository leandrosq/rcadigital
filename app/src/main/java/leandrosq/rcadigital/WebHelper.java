package leandrosq.rcadigital;

import android.net.Uri;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

/**
 * Created by Leandro SQ on 18/09/2017.
 */

public class WebHelper {

    public static String getStreamData (InputStream stream) {
        Scanner s = new Scanner (stream, "UTF-8").useDelimiter ("\\A");
        return s.hasNext () ? s.next () : "";
    }

    public static String makeRequest (Uri url) throws IOException {
        String buffer = null;
        HttpURLConnection connection = (HttpURLConnection) new URL (url.toString ()).openConnection ();
        try {
            InputStream in = new BufferedInputStream (connection.getInputStream ());
            buffer = getStreamData (in);
        } finally {
            connection.disconnect ();
        }

        return buffer;
    }
}
