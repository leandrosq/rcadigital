package leandrosq.rcadigital;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private EditText inputEmail, inputPassword;
    private Button buttonSignIn;


    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_main);
        //Toolbar
        Toolbar toolbar = (Toolbar) findViewById (R.id.main_toolbar);
        setSupportActionBar (toolbar);
        getSupportActionBar ().setDisplayShowTitleEnabled (false);
        //Ui elements
        inputEmail = (EditText) findViewById (R.id.input_email);
        inputPassword = (EditText) findViewById (R.id.input_password);
        buttonSignIn = (Button) findViewById (R.id.button_signin);
        //Action
        buttonSignIn.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick (View v) {
                if (inputEmail.getText ().toString ().equals (inputPassword.getText ().toString ())) {
                    Intent intent = new Intent (MainActivity.this, ExtractActivity.class);
                    startActivity (intent);
                } else {
                    AlertDialog.Builder dialog = new AlertDialog.Builder (MainActivity.this);
                    dialog.setIcon (R.mipmap.ic_launcher)
                            .setPositiveButton ("OK", null)
                            .setTitle (getResources ().getString (R.string.error))
                            .setMessage (getResources ().getString (R.string.invalid_credentials))
                            .show ();
                }
            }
        });
    }
}
