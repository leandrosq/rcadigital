package leandrosq.rcadigital;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;

/**
 * Created by Leandro SQ on 18/09/2017.
 */

public class Extract {

    public String pastDue;
    public String carnet;
    public String installment;
    public String value;
    public Detail detail;

    public Extract (String pastDue, String carnet, String installment, String value, Detail detail) {
        this.pastDue = pastDue;
        this.carnet = carnet;
        this.installment = installment;
        this.value = value;
        this.detail = detail;
    }

    public Extract (Context c, JSONObject jsonObject) throws JSONException {
        pastDue = jsonObject.getString ("past_due");
        String[] buffer = pastDue.split ("/");
        pastDue = buffer[0] + " " + c.getResources ().getStringArray (R.array.months)[Integer.parseInt (buffer[1]) - 1];

        carnet = jsonObject.getString ("carnet");

        installment = jsonObject.getString ("installment");
        buffer = installment.split ("/");
        installment = "<b>" + buffer[0] + "</b>/" + buffer[1];

        value = jsonObject.getString ("value");
        detail = new Detail (jsonObject.getJSONObject ("detail"));
    }

    public String getPastDue () {
        return pastDue;
    }

    public void setPastDue (String pastDue) {
        this.pastDue = pastDue;
    }

    public String getCarnet () {
        return carnet;
    }

    public void setCarnet (String carnet) {
        this.carnet = carnet;
    }

    public String getInstallment () {
        return installment;
    }

    public void setInstallment (String installment) {
        this.installment = installment;
    }

    public String getValue () {
        return value;
    }

    public void setValue (String value) {
        this.value = value;
    }

    public Detail getDetail () {
        return detail;
    }

    public void setDetail (Detail detail) {
        this.detail = detail;
    }
}
