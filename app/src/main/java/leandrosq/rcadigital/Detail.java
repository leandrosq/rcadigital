package leandrosq.rcadigital;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Leandro SQ on 18/09/2017.
 */

public class Detail {

    public String overdueDays;
    public String overdueDate;
    public String originalValue;
    public String valueDiff;
    public String totalValue;
    public String store;

    public Detail (String overdueDays, String overdueDate, String originalValue, String valueDiff, String totalValue, String store) {

        this.overdueDays = overdueDays;
        this.overdueDate = overdueDate;
        this.originalValue = originalValue;
        this.valueDiff = valueDiff;
        this.totalValue = totalValue;
        this.store = store;
    }

    public Detail (JSONObject jsonObject) throws JSONException {
        overdueDays = jsonObject.getString ("overdue_days");
        overdueDate = jsonObject.getString ("overdue_date");
        originalValue = jsonObject.getString ("original_value");
        valueDiff = jsonObject.getString ("value_diff");
        totalValue = jsonObject.getString ("total_value");
    }

    public String getOverdueDays () {
        return overdueDays;
    }

    public void setOverdueDays (String overdueDays) {
        this.overdueDays = overdueDays;
    }

    public String getOverdueDate () {
        return overdueDate;
    }

    public void setOverdueDate (String overdueDate) {
        this.overdueDate = overdueDate;
    }

    public String getOriginalValue () {
        return originalValue;
    }

    public void setOriginalValue (String originalValue) {
        this.originalValue = originalValue;
    }

    public String getValueDiff () {
        return valueDiff;
    }

    public void setValueDiff (String valueDiff) {
        this.valueDiff = valueDiff;
    }

    public String getTotalValue () {
        return totalValue;
    }

    public void setTotalValue (String totalValue) {
        this.totalValue = totalValue;
    }

    public String getStore () {
        return store;
    }

    public void setStore (String store) {
        this.store = store;
    }
}
