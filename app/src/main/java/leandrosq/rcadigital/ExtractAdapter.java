package leandrosq.rcadigital;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by Leandro SQ on 18/09/2017.
 */

public class ExtractAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Extract> data;
    private static LayoutInflater inflater = null;

    public ExtractAdapter (Context context, ArrayList<Extract> data) {
        this.context = context;
        this.data = data;
        inflater = (LayoutInflater) context.getSystemService (Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount () {
        return data.size ();
    }

    @Override
    public Object getItem (int position) {
        return data.get (position);
    }

    @Override
    public long getItemId (int position) {
        return position;
    }

    @Override
    public View getView (int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        if (vi == null) vi = inflater.inflate (R.layout.extract_row, null);

        CheckBox checkBox = (CheckBox) vi.findViewById (R.id.extract_select);
        TextView eDate = (TextView) vi.findViewById (R.id.extract_date);
        TextView eCarnet = (TextView) vi.findViewById (R.id.extract_carnet);
        TextView eInstallment = (TextView) vi.findViewById (R.id.installment);
        TextView eValue = (TextView) vi.findViewById (R.id.extract_value);

        Extract e = data.get (position);

        eDate.setText (e.getPastDue ());
        eCarnet.setText (e.getCarnet ());
        eInstallment.setText (Html.fromHtml (e.getInstallment ()));
        eValue.setText (e.getValue ());

        if (getCount () - 1 == position) {
            vi.setPadding (0, 0, 0, 50);
        } else {
            vi.setPadding (0, 0, 0, 0);
        }

        return vi;
    }
}
