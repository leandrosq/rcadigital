package leandrosq.rcadigital;

import android.app.ProgressDialog;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class ExtractActivity extends AppCompatActivity implements Runnable {

    private final String REST_URL="http://www.icoded.com.br/api/extract.php?pwd=123456";

    private TextView tvUserName, tvUserBalance, tvUserLimit, tvUserUsed;
    private ListView lvExtract;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_extract);
        //Toolbar
        Toolbar toolbar = (Toolbar) findViewById (R.id.main_toolbar);
        setSupportActionBar (toolbar);
        getSupportActionBar ().setDisplayHomeAsUpEnabled (true);
        getSupportActionBar ().setDisplayShowTitleEnabled (false);
        //Ui elements
        tvUserName = (TextView) findViewById (R.id.user_name);
        tvUserBalance = (TextView) findViewById (R.id.user_balance);
        tvUserLimit = (TextView) findViewById (R.id.user_limit);
        tvUserUsed = (TextView) findViewById (R.id.user_used);
        lvExtract = (ListView) findViewById (R.id.extract_list);
        //Action
        lvExtract.setDivider (null);
        lvExtract.setDividerHeight (2);
        lvExtract.setChoiceMode (ListView.CHOICE_MODE_SINGLE);

        lvExtract.setOnItemClickListener (new AdapterView.OnItemClickListener () {
            @Override
            public void onItemClick (AdapterView<?> parent, View view, int position, long id) {
                Extract extract = (Extract) lvExtract.getAdapter ().getItem (position);

                AlertDialog.Builder builder = new AlertDialog.Builder (ExtractActivity.this);
                builder.setIcon (R.mipmap.ic_launcher)
                        .setPositiveButton ("OK", null)
                        .setTitle (tvUserName.getText ())
                        .setMessage ("Data da parcela: " + extract.getDetail ().getOverdueDate ())
                        .show ();

                CheckBox cb = (CheckBox) view.findViewById(R.id.extract_select);
                cb.setChecked (true);
            }
        });

        progressDialog = ProgressDialog.show (ExtractActivity.this, "Por favor, espere.", "Processando dados...", true, true);
        new Thread (this).start ();
    }

    private void populateStaticElements (String username, String balance, String limit, String used) {
        tvUserName.setText (username);
        tvUserBalance.setText (balance);
        tvUserLimit.setText (limit);
        tvUserUsed.setText (used);
    }

    private void populateExtractList (ArrayList<Extract> extractList) {
        ExtractAdapter adapter = new ExtractAdapter (ExtractActivity.this, extractList);
        lvExtract.setAdapter (adapter);
        progressDialog.dismiss ();
    }

    @Override
    public void run () {
        try {
            //Make web request
            String buffer = WebHelper.makeRequest (Uri.parse (REST_URL));
            //Turn to JSON Object
            JSONObject data = new JSONObject (buffer).getJSONObject ("data");
            JSONObject limits = data.getJSONObject ("limits");
            JSONArray installments = data.getJSONArray ("installments");

            final String username = data.getString ("name");
            final String balance = limits.getString ("available");
            final String limit = limits.getString ("total");
            final String used = limits.getString ("expent");

            final ArrayList<Extract> extractList = new ArrayList<> ();

            for (int i = 0; i < installments.length (); i++) {
                extractList.add (new Extract (ExtractActivity.this, installments.getJSONObject (i)));
            }

            //Populate static elements
            runOnUiThread (new Runnable () {
                @Override
                public void run () {
                    populateStaticElements (username, balance, limit, used);
                    populateExtractList (extractList);
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace ();
        }
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item) {
        if (item.getItemId () == android.R.id.home) finish ();

        return super.onOptionsItemSelected (item);
    }
}
